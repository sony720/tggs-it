from openerp import fields,models
class Service(models.Model):
	_name = 'service.service'
	description = fields.Text(string='Result Description', size=64)
    progress = fields.Char(string='Progress')
    time = fields.Date(string='Delivery date')
    accepter = fields.Char(string='Accepted By',size=32)
    responsibleperson = fields.Char(string='Responsible Person',size=32)
    check = fields.Char(string='Checked By',size=32)