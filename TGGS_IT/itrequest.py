from openerp import fields,models

class ItRequest(models.Model):
	_name = "itrequest.itrequest"

	customer_id = fields.Integer(string="Customer ID")
	name = fields.Char(string='Name',size=64)
	date = fields.Date(string='Date')
	department = fields.Char(string='Department',size=64)
	telephone = fields.Integer(string='Telephone')
	email = fields.Char(string='Email',size=64)
	jobdescription = fields.Text(string='Job Description',size=50)
	location = fields.Char(string='Location',size=64)
	requestedby = fields.Char(string='Requested By',size=64)
	responsibleperson = fields.Char(string='Responsible Person',size=64)
	attached = fields.Many2many('ir.attachment','rel_attached',string='Attached Document')

class Attachment(models.Model):
	_name = 'itrequest.attachment'
	_rec_name = "name"
	name = fields.Char(size=32,string="Name")
	sks = fields.Integer(string="SKS")
    		